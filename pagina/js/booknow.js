$(function(){
    //when the data updates at firebase, put it in the data variable
    //data= snapshot.val();
    })

//Entire Form (handler)
$('#newReservation').submit(function(event) {

    //create new connection to firebase
    var config = {
        apiKey: "AIzaSyAeeFajTkDxAnxXLCzjSyLJq86reuxDgnE",
        authDomain: "recepcion-hotelera.firebaseapp.com",
        databaseURL: "https://recepcion-hotelera.firebaseio.com",
        storageBucket: "recepcion-hotelera.appspot.com",
    };
    firebase.initializeApp(config);



    var $form = $(this);
    console.log("Submit to Firebase");

    //disable submit button
    $form.find("#saveForm").prop('disabled', true);

    //get values to send to Firebase
    var aDateToSend = $('#arrival_date').val();
    console.log(aDateToSend);

    var dDateToSend = $('#departure-date').val();
    console.log(dDateToSend);

    var statusToSend = "Por confirmar".val();
    console.log(statusToSend);

    var todayToSend = new Date();
    var dd = todayToSend.getDate();
    var mm = todayToSend.getMonth()+1; //January is 0!
    var yyyy = todayToSend.getFullYear();

    if(dd<10) {
        dd = '0'+dd
    }

    if(mm<10) {
        mm = '0'+mm
    }
    today = dd + '/' + mm + '/' + yyyy;
    console.log(todayToSend);

    var roomToSend= $('#room').val();
    console.log(roomToSend);

    var roomTypeToSend = $('#roomtype').val();
    console.log(roomTypeToSend);

    var guestToSend = $('#guest').val();
    console.log(guestToSend);

    var mailToSend = $('#email').val();
    console.log(mailToSend);

    var messageToSend = $('#message').val();
    console.log(messageToSend);

    // Get a reference to the database service
    var database = firebase.database().ref("/");
    console.log(database);

    firebase.database().ref('reservas/' + 3).set({
            "checkIn" : aDateToSend,
            "checkOut" : dDateToSend,
            "estado" : statusToSend,
            "fecha" : todayToSend,
            "metodo" : "Página",
            "saldo" : 0
    });

    /*var newReservation= {
        "reservas" : [null, {
            "checkIn" : aDateToSend,
            "checkOut" : dDateToSend,
            "estado" : "Por confirmar",
            "fecha" : todayToSend,
            "habitacionesClientes" : [ null,{
                "checkInReal" : "",
                "checkOutReal" : "",
                "codigoHabitacion" : "",
                "consumo" : 0,
                "esTitular" : true,
                "idCliente" : 0,
                pagado : 0
                } ],
            "metodo" : "Página",
            "saldo" : 0
            } ]
    }*/

    //put new object in data array
    /*data.push(newReservation);
    console.log(data);*/

    //send the new data to Firebase
    /*ref.set(data, function(err){
    if(err){
        alert("Data no go");
        }
    });*/

    return false;
    })


