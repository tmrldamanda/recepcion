import { Component, OnInit } from '@angular/core';
import { AuthService } from '../servicios/auth.service';
import {Router, ActivatedRoute } from '@angular/router';
import {FlashMessagesService} from 'angular2-flash-messages';

import { FormBuilder, FormGroup } from '@angular/forms';
import { FirebaseService } from '../core/data/firebase.service';
import { Usuario } from 'src/app/core/data/data.model';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public email: string;
  public password: string;
  public nombre_completo:string;
  form: FormGroup;

  constructor(
    public authService: AuthService,
    public router: Router,
    public flashMensaje: FlashMessagesService,
    private builder: FormBuilder,
    private firebase: FirebaseService,
    private actRoute: ActivatedRoute

  ) { }

  ngOnInit() {
    this.form=this.builder.group({
      nombre_completo:'',
      email: ''
    })
  }

  onSubmitAddUser() {
    this.authService.registerUser(this.email, this.password)
    .then((res) => {
      console.log('bien!');
      this.flashMensaje.show('Usuario creado correctamente.',
      {cssClass: 'alert-success', timeout: 4000});
     this.router.navigate(['/home']);
    }).catch( (err) => {
      this.flashMensaje.show(err.message,
      {cssClass: 'alert-danger', timeout: 4000});
    });
//00000000000000000000000000000000
      var nombre_completo = this.form.value;
      var email = this.form.value.email;
      this.firebase.addUsuario(nombre_completo, email as Usuario);
      this.router.navigate(['..'], {relativeTo: this.actRoute});



  }
}
