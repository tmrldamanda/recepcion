import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusquedaCheckInComponent } from './busqueda-check-in.component';

describe('BusquedaCheckInComponent', () => {
  let component: BusquedaCheckInComponent;
  let fixture: ComponentFixture<BusquedaCheckInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusquedaCheckInComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusquedaCheckInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
