import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BusquedaCheckInComponent } from './busqueda-check-in/busqueda-check-in.component';

const routes: Routes = [{
  path: '',
  component: BusquedaCheckInComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CheckInRoutingModule { }