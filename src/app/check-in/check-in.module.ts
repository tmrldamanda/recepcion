import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CheckInRoutingModule } from './check-in-routing.module';
import { BusquedaCheckInComponent } from './busqueda-check-in/busqueda-check-in.component';

@NgModule({
  imports: [
    CommonModule,
    CheckInRoutingModule
  ],
  declarations: [BusquedaCheckInComponent]
})
export class CheckInModule { }
