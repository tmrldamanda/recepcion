import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import {LoginComponent} from './+login/login.component';
import {RegisterComponent} from './+register/register.component';
//Para verificar si esta logueado
import {AuthGuard} from './guards/auth.guard';

const routes: Routes = [
  {
  path: '',
  data: {
      title: 'Menú Principal'
  },
  children: [
    {
      path: '',
      component: HomeComponent
    }, {
      path: 'reservas',
      //Solo se podra ingresar a reservas si se esta logueado
      canActivate: [AuthGuard],
      loadChildren: './reservas/reservas.module#ReservasModule',
      data: {
        title: 'Reservas'
      }
    }, {
      path: 'habitaciones',
      //Solo se podra ingresar a habitaciones si se esta logueado
      canActivate: [AuthGuard],
      loadChildren: './habitaciones/habitaciones.module#HabitacionesModule',
      data: {
        title: 'Habitaciones'
      }
    }, {
      path: 'check-in',
      //Solo se podra ingresar a check-in si se esta logueado
       canActivate: [AuthGuard],
      loadChildren: './check-in/check-in.module#CheckInModule',
      data: {
        title: 'Check In'
      }
    }, {
      path: 'check-out',
       //Solo se podra ingresar a check-out si se esta logueado
      canActivate: [AuthGuard],
      loadChildren: './check-out/check-out.module#CheckOutModule',
      data: {
        title: 'Check Out'
      }
    }, {
      path: 'login', component: LoginComponent},{
        path: 'register', component: RegisterComponent},
  ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
