import { Injectable } from '@angular/core';

import { AngularFireDatabase, AngularFireList, AngularFireObject } from 'angularfire2/database';
import { Habitacion } from 'src/app/core/data/data.model';
import { Usuario } from 'src/app/core/data/data.model';


@Injectable()
export class FirebaseService {

  prueba: AngularFireObject<any>;
  habitaciones: AngularFireList<any>;
  habitacion: AngularFireObject<Habitacion>;

  usuarios: AngularFireList<any>;
  usuario:AngularFireObject<Usuario>;

  constructor(private firebase: AngularFireDatabase) { }

  getPrueba() {
    this.prueba = this.firebase.object('/prueba');
    return this.prueba;
  }

  getHabitaciones() {
    this.habitaciones = this.firebase.list('/habitaciones');
    return this.habitaciones;
  }

  getHabitacionPorCodigo(codigo: string) {
    this.habitacion = this.firebase.object('/habitaciones/'+codigo);
    return this.habitacion;
  }

  addHabitacion(id: string, habitacion: Habitacion) {
    this.habitaciones.set(`${id}`, habitacion);
  }

  addUsuario(id: string, usuario: Usuario) {
    this.usuarios.set(`${id}`, usuario);
  }

  deleteHabitacion(id: string) {
    this.habitaciones.remove(id);
  }
  /*actuatorList: AngularFireList<any>;
  sensorList: AngularFireList<any>;
  isServerActive: AngularFireObject<any>;

  constructor(private firebase: AngularFireDatabase) {
  }

  getActive(id) {
    this.isServerActive = this.firebase.object('group/'+id+'/active');
    return this.isServerActive;
  }

  getActuatorsData(id) {
    this.actuatorList = this.firebase.list('group/'+id+'/actuator');
    return this.actuatorList;
  }

  getSensorsData(id) {
    this.sensorList = this.firebase.list('group/'+id+'/sensor');
    return this.sensorList;
  }

  updateLightRGB(device: LightRGB, param: string) {
    var value = (param == 'brightness') ? device[param]*255/100.0 : device[param];
    this.actuatorList.set(`${device.$key}/${param}`, value);
  }

  updateLightWTA(device: LightWTA, param: string) {
    var value = (param == 'brightness') ? device[param]*255/100.0 : device[param];
    this.actuatorList.set(`${device.$key}/${param}`, value);
  }

  updateSwitch(device: Switch) {
    this.actuatorList.set(`${device.$key}/state`, device.state);
  }

  deleteEmployee($key : string){
    this.deviceList.remove($key);
  }*/

}