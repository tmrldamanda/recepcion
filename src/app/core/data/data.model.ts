export class Reserva {
    idReserva: number;
    checkIn: Date;
    checkOut: Date;
    estado: string;
    saldo: number;
    fecha: Date;
    habitacionesClientes: HabitacionCliente[];
    facturas: Factura[];
}

export class Habitacion {
    codigo: string;
    piso: string;
    numero: string;
    estado: string;
    capacidad: number;
    tipo: string;
    precio: number
}

export class Usuario {
    codigo: string;
    nombre_completo: string;
    email: string;
}

export class Cliente {
    idCliente: number;
    nombres: string;
    apellidos: string;
    documento: number;
    tipoDocumento: string;
    correo: string;
    esEjecutivo: boolean
}

export class Peticion {
    idPeticion: number;
    descripcion: string;
    costoAdicional: number;
}

export class HabitacionCliente {
    idHabCte: number;
    checkInReal: Date;
    checkOutReal: Date;
    consumo: number;
    pagado: number;
    idCliente: number;
    idHabitacion: number;
    peticiones?: number[]; // [0, 1, 5]: Almacenan IDs de Peticiones
}

export class Factura {
    idFactura: number;
    nombre: string;
    nit: number;
    detalle: JSON;
    fecha: Date
}