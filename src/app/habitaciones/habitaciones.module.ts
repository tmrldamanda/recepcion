import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HabitacionesRoutingModule } from './habitaciones.routing.module';
import { PanelHabitacionesComponent } from './panel-habitaciones/panel-habitaciones.component';
import { AgregarHabitacionComponent } from './agregar-habitacion/agregar-habitacion.component';
import { ModificarHabitacionComponent } from './modificar-habitacion/modificar-habitacion.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    HabitacionesRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [PanelHabitacionesComponent, AgregarHabitacionComponent, ModificarHabitacionComponent]
})
export class HabitacionesModule { }
