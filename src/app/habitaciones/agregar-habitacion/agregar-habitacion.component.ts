import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FirebaseService } from './../../core/data/firebase.service';
import { Habitacion } from 'src/app/core/data/data.model';

@Component({
  selector: 'app-agregar-habitacion',
  templateUrl: './agregar-habitacion.component.html',
  styleUrls: ['./agregar-habitacion.component.css']
})
export class AgregarHabitacionComponent implements OnInit {

  form: FormGroup;

  constructor(private builder: FormBuilder,
              private actRoute: ActivatedRoute,
              private firebase: FirebaseService,
              private router: Router) { }

  ngOnInit() {
    this.form = this.builder.group({
      codigo: '',
      piso: '',
      numero: '',
      tipo: '',
      precio: '',
      capacidad: ''
    })
  }

  submitHabitacion() {
    var habitacion = this.form.value;
    var codigo = this.form.value.codigo;
    delete habitacion['codigo'];
    habitacion['estado'] = 'Disponible';
    this.firebase.addHabitacion(codigo, habitacion as Habitacion);
    this.router.navigate(['..'], {relativeTo: this.actRoute});
  }

}
