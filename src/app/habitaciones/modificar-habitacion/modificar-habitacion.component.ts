import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FirebaseService } from 'src/app/core/data/firebase.service';
import { Habitacion } from 'src/app/core/data/data.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-modificar-habitacion',
  templateUrl: './modificar-habitacion.component.html',
  styleUrls: ['./modificar-habitacion.component.css']
})
export class ModificarHabitacionComponent implements OnInit {

  form: FormGroup;
  habitacion: Habitacion;
  subscription: Subscription;
  codigo: string;

  constructor(private builder: FormBuilder,
              private actRoute: ActivatedRoute,
              private firebase: FirebaseService,
              private router: Router) { }

  ngOnInit() {
    this.codigo = this.actRoute.snapshot.params['codigo'];
    
    this.subscription = this.firebase.getHabitacionPorCodigo(this.codigo).snapshotChanges()
    .subscribe(item => {
      this.habitacion = item.payload.toJSON() as Habitacion;
      this.habitacion['codigo'] = item.key;
      this.habitacion;
      
      this.form = this.builder.group({
        codigo: this.habitacion.codigo,
        piso: this.habitacion.piso,
        numero: this.habitacion.numero,
        tipo: this.habitacion.tipo,
        precio: this.habitacion.precio,
        capacidad: this.habitacion.capacidad
      });
    });
  }

  submitHabitacion() {
    var habitacion = this.form.value;
    var codigo = this.form.value.codigo;
    delete habitacion['codigo'];
    habitacion['estado'] = 'Disponible';
    this.firebase.addHabitacion(codigo, habitacion as Habitacion);
    this.subscription.unsubscribe();
    this.router.navigate(['../..'], {relativeTo: this.actRoute});
  }

}
