import { Component, OnInit } from '@angular/core';

import { FirebaseService } from "../../core/data/firebase.service";
import { Habitacion } from 'src/app/core/data/data.model';

@Component({
  selector: 'app-panel-habitaciones',
  templateUrl: './panel-habitaciones.component.html',
  styleUrls: ['./panel-habitaciones.component.css']
})
export class PanelHabitacionesComponent implements OnInit {

  headers: string[] = ['Código', 'Tipo', 'Precio p/Noche', 'Estado', 'Piso', 'Numero', 'Modificar', 'Eliminar'];
  habitaciones: Habitacion[];

  constructor(private firebase: FirebaseService) { }

  ngOnInit() {
    this.firebase.getHabitaciones().snapshotChanges()
    .subscribe(item => {
      this.habitaciones = [];

      item.forEach(element => {
        var habitacion = element.payload.toJSON();
        habitacion['codigo'] = element.key;
        this.habitaciones.push(habitacion as Habitacion);
      });
    })
  }

  deleteClick(codigo) {
    this.firebase.deleteHabitacion(codigo);
  }

}
