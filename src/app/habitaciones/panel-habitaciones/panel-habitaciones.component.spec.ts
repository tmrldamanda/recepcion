import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelHabitacionesComponent } from './panel-habitaciones.component';

describe('PanelHabitacionesComponent', () => {
  let component: PanelHabitacionesComponent;
  let fixture: ComponentFixture<PanelHabitacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelHabitacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelHabitacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
