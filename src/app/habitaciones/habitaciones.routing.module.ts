import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PanelHabitacionesComponent } from './panel-habitaciones/panel-habitaciones.component';
import { AgregarHabitacionComponent } from './agregar-habitacion/agregar-habitacion.component';
import { ModificarHabitacionComponent } from './modificar-habitacion/modificar-habitacion.component';

const routes: Routes = [
  {
    path: '',
    component: PanelHabitacionesComponent
  },
  {
    path: 'agregar-habitacion',
    component: AgregarHabitacionComponent,
    data: {
      title: 'Agregar Habitación'
    }
  },
  {
    path: 'modificar-habitacion/:codigo',
    component: ModificarHabitacionComponent,
    data: {
      title: 'Modificar Habitación'
    }
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HabitacionesRoutingModule { }