import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusquedaCheckOutComponent } from './busqueda-check-out.component';

describe('BusquedaCheckOutComponent', () => {
  let component: BusquedaCheckOutComponent;
  let fixture: ComponentFixture<BusquedaCheckOutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusquedaCheckOutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusquedaCheckOutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
