import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BusquedaCheckOutComponent } from './busqueda-check-out/busqueda-check-out.component';

const routes: Routes = [{
  path: '',
  component: BusquedaCheckOutComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CheckOutRoutingModule { }