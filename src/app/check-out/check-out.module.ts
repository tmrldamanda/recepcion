import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CheckOutRoutingModule } from './check-out-routing.module';
import { BusquedaCheckOutComponent } from './busqueda-check-out/busqueda-check-out.component';
import { FacturacionComponent } from './facturacion/facturacion.component';

@NgModule({
  imports: [
  CommonModule,
  CheckOutRoutingModule
  ],
  declarations: [BusquedaCheckOutComponent, FacturacionComponent]
})
export class CheckOutModule { }
