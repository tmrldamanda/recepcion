import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelReservasComponent } from './panel-reservas.component';

describe('PanelReservasComponent', () => {
  let component: PanelReservasComponent;
  let fixture: ComponentFixture<PanelReservasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelReservasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelReservasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
