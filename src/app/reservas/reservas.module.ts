import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReservasRoutingModule } from './reservas-routing.module';
import { AgregarReservaComponent } from './agregar-reserva/agregar-reserva.component';
import { ModificarReservaComponent } from './modificar-reserva/modificar-reserva.component';
import { PanelReservasComponent } from './panel-reservas/panel-reservas.component';

@NgModule({
  imports: [
    CommonModule,
    ReservasRoutingModule
  ],
  declarations: [AgregarReservaComponent, ModificarReservaComponent, PanelReservasComponent]
})
export class ReservasModule { }
