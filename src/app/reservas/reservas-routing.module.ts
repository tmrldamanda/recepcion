import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PanelReservasComponent } from './panel-reservas/panel-reservas.component';

const routes: Routes = [{
  path: '',
  component: PanelReservasComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReservasRoutingModule { }
