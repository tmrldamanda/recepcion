import { Component, AfterViewInit } from '@angular/core';

import * as Prism from 'prismjs';
import { FirebaseService } from './../core/data/firebase.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements AfterViewInit {

  prueba: any = 'Hello from a Variable :(';
  /**
   * @method ngAfterViewInit
   */

   constructor (private firebase: FirebaseService) {}

  ngAfterViewInit() {
    Prism.highlightAll();
  }

  ngOnInit() {
    this.firebase.getPrueba().snapshotChanges()
    .subscribe(item => {
        this.prueba = item.payload.toJSON();
        });
  }
}
